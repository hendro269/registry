# Registry

[Distributed Hash Table](https://en.wikipedia.org/wiki/Distributed_hash_table) (DHT) implementation for the Fluence network with an Aqua interface.

## Documentation
See [Aqua Book](https://fluence.dev/aqua-book/libraries/aqua-dht).

## How to Use

See [example](./example):
- How to call [`registry`](./example/src/example.ts) function in TS/JS
- Writing an Aqua script using `registry`: [event_example.aqua](./example/src/aqua/event_example.aqua)

## API

API is defined in the [routing.aqua](./aqua/routing.aqua) module.

## Learn Aqua

* [Aqua Book](https://fluence.dev/aqua-book/)
* [Aqua Playground](https://github.com/fluencelabs/aqua-playground)
* [Aqua repo](https://github.com/fluencelabs/aqua)
